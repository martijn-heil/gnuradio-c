#ifndef GRC_MSG_ACCEPTER_H
#define GRC_MSG_ACCEPTER_H

#include <stdbool.h>
#include <stddef.h>

#include <grc/pmt.h>

#include "namespace.h" // should be last include

#define Self NAMESPACE ## _MsgAccepter
#define _methodprefix NAMESPACE ## _msg_accepter

#define VIRTUAL_METHODS \
    method1(void, post, grc_pmt_t which_port, grc_pmt_t msg); \

#define METHODS \
    VIRTUAL_METHODS

#ifdef __cplusplus
extern "C" {
#endif

#define method0(...) _funcptr_method0(__VA_ARGS__)
#define method1(...) _funcptr_method1(__VA_ARGS__)
DECLARE_CLASS_VTABLE(Self, VIRTUAL_METHODS)

#define method0(...) _func_method0(__VA_ARGS__)
#define method1(...) _func_method1(__VA_ARGS__)
DECLARE_CLASS_FUNCTIONS(Self, METHODS)

#ifdef __cplusplus
}
#endif

#undef _methodprefix
#undef Self
#undef NAMESPACE

#endif
