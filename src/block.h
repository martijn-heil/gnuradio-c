#ifndef GNURADIO_C_BLOCK_H
#define GNURADIO_C_BLOCK_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include <grc/mod.h>

#include "class.h"
#include "namespace.h" // should be last include

#define TagPropagationPolicy grc_TagPropagationPolicy

#define Self NAMESPACE ## _Block
#define _methodprefix NAMESPACE ## _block_

#define VIRTUAL_METHODS \
    // virtual public member functions
    method1(void, forecast, int noutput_items, int *ninput_items_required, size_t n); \
    method1(int, general_work, int noinput_items, int *ninput_items, size_t ninput_items_size, \
        const void **input_items, size_t input_items_size, void **output_items, size_t output_items_size); \
    method0(bool, start); \
    method0(bool, stop); \
    method1(int, fixed_rate_ninput_to_noutput, int ninput); \
    method1(int, fixed_rate_noutput_to_ninput, int noutput);
    // end virtual public member functions

#define METHODS \
    VIRTUAL_METHODS \
    method0(unsigned int, history); \
    method0(void, set_history); \
    method1(void, declare_sample_delay_which, int which, unsigned int delay); \
    method1(void, declare_sample_delay, unsigned int delay); \
    method0(bool, fixed_rate); \
    method1(void, set_output_multiple, int multiple); \
    method0(int, output_multiple); \
    method0(bool, output_multiple_set); \
    method1(void, set_alignment, int multiple); \
    method0(int, unaligned); \
    method0(bool, is_unaligned); \
    method1(void, consume, int which_input, int how_many_items); \
    method1(void, consume_each, int how_many_items); \
    method1(void, produce, int which_output, int how_many_items); \
    method1(void, set_relative_rate, double relative_rate); \
    method1(double, relative_rate, double relative_rate); \
    method1(uint64_t, nitems_read, unsigned int which_input); \
    method1(uint64_t, nitems_written, unsigned int which_output); \
    method0(enum TagPropagationPolicy, tag_propagation_policy); \
    method1(void, set_tag_propagation_policy, enum TagPropagationPolicy p); \
    method0(int, min_noutput_items); \
    method1(void, set_min_noutput_items, int m); \
    method0(int, max_noutput_items); \
    method1(void, set_max_noutput_items, int m); \
    method0(void, unset_max_noutput_items); \
    method0(bool, is_set_max_noutput_items); \
    method1(void, expand_minmax_buffer, int port); \
    method1(long, max_output_buffer, size_t i); \
    method0(void, set_max_output_buffer, long max_output_buffer); \
    method1(void, set_max_output_buffer_with_port(int port, long max_output_buffer); \
    method1(long, min_output_buffer, size_t i); \
    method1(void, set_min_output_buffer, long min_output_buffer); \
    method0(float, pc_noutput_items); \
    method0(float, pc_noutput_items_avg); \
    method0(float, pc_noutput_items_var); \
    method0(float, pc_nproduced); \
    method0(float, pc_nproduced_avg); \
    method0(float, pc_nproduced_var); \
    method1(float, pc_input_buffers_full, int which); \
    method1(float, pc_input_buffers_full_avg, int which); \
    method1(float, pc_input_buffers_full_var, int which); \
    // TODO block_pc_input_buffers_full_* overloaded functions
    method1(float, pc_output_buffers_full, int which); \
    method1(float, pc_output_buffers_full_avg, int which); \
    method1(float, pc_output_buffers_full_var, int which); \
    // TODO block_pc_output_buffers_full_* overloaded functions
    method0(float, pc_work_time); \
    method0(float, pc_work_time_avg); \
    method0(float, pc_work_time_var); \
    method0(float, pc_work_time_total); \

    method0(float, pc_throughput_avg); \
    method0(void, reset_perf_counters); \
    method0(void, setup_pc_rpc); \
    method0(bool, is_pc_rpc_set); \
    method0(void, no_pc_rpc); \
    method0(void, set_processor_affinity, int *mask, size_t n); \
    method0(void, unset_processor_affinity); \
    method0(int, active_thread_priority); \
    method0(int, thread_priority); \
    method1(int, set_thread_priority, int priority); \
    method0(bool, update_rate); \
    // TODO void block_system_handler()
    method0(bool, finished); \
    // TODO block_detail & set_detail
    method0(void, notify_msg_neighbors); \
    method0(void, clear_finished);

#ifdef __cplusplus
extern "C" {
#endif

#define method0(...) _funcptr_method0(__VA_ARGS__)
#define method1(...) _funcptr_method1(__VA_ARGS__)
DECLARE_CLASS_VTABLE(Self, VIRTUAL_METHODS)

#define method0(...) _func_method0(__VA_ARGS__)
#define method1(...) _func_method1(__VA_ARGS__)
DECLARE_CLASS_FUNCTIONS(Self, METHODS)

typedef struct Self {
    struct _vtable_ ## Self vtable;
    grc_BasicBlock _super_BasicBlock;

    long *_protected_d_max_output_buffer;
    size_t _protected_d_max_output_buffer_size;
    long *_protected_d_min_output_buffer;
    size_t _protected_d_min_output_buffer_size;
    grc_Mutex _protected_d_setlock;
    void *_protected_d_logger;
    void *_protected_d_debug_logger;
    grc_pmt _protected_d_pmt_done;
    grc_pmt _protected_d_system_port;
    
    void *_data;
} Self;

#ifdef __cplusplus
}
#endif

#undef _methodprefix
#undef Self
#undef NAMESPACE

#endif
