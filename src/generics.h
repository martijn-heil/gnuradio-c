#ifndef GRC_GENERICS_H
#define GRC_GENERICS_H

#ifndef __cplusplus
#define ascciify_type(T) _Generic((T), \
        void: void,
        void*: voidstar,
        void**: voidstarstar,
        const void*: constvoidstar,
        const void**: constvoidstarstar,

        char: char,
        char*: charstar,
        char**: charstarstar,
        const char*: constcharstar,
        const char**: constcharstarstar,

        unsigned char: unsignedchar,
        unsigned char*: unsignedcharstar,
        unsigned char**: unsignedcharstarstar,
        const unsigned char*: constunsignedcharstar,
        const unsigned char**: constunsignedcharstarstar,

        short: short,
        short*: shortstar,
        short**: shortstarstar,
        const short*: constshortstar,
        const short**: constshortstarstar,

// TODO the other unsigned types

        int: int,
        int*: intstar,
        int**: intstarstar,
        const int*: constintstar,
        const int**: constintstarstar,

        long: long
        long*: longstar,
        long**: longstarstar,
        const long*: constlongstar,
        const long**: constlongstarstar,

        long long: longlong,
        long long*: longlongstar,
        long long**: longlongstarstar,
        const long long*: constlonglongstar,
        const long long**: constlonglongstarstar,

        float: float,
        float*: floatstar,
        float**: floatstarstar,
        const float*: constfloatstar,
        const float**: constfloatstarstar,

        double: double,
        double*: doublestar,
        double**: doublestarstar,
        const double*: constdoublestar,
        const double**: constdoublestarstar,

        uint8_t: uint8_t,
        uint8_t*: uint8_tstar,
        uint8_t**: uint8_tstarstar,
        const uint8_t*: constuint8_tstar,
        const uint8_t**: constuint8_tstarstar,

        uint16_t: uint16_t,
        uint16_t*: uint16_tstar,
        uint16_t**: uint16_tstarstar,
        const uint16_t*: constuint16_tstar,
        const uint16_t**: constuint16_tstarstar,

        uint32_t: uint32_t,
        uint32_t*: uint32_tstar,
        uint32_t**: uint32_tstarstar,
        const uint32_t*: constuint32_tstar,
        const uint32_t**: constuint32_tstarstar,

        uint64_t: uint64_t,
        uint64_t*: uint64_tstar,
        uint64_t**: uint64_tstarstar,
        const uint64_t*: constuint64_tstar,
        const uint64_t**: constuint64_tstarstar,

        int8_t: int8_t,
        int8_t*: int8_tstar,
        int8_t**: int8_tstarstar,
        const int8_t*: constint8_tstarstar,
        const int8_t**: constint8_tstarstar,

        int16_t: int16_t,
        int16_t*: int16_tstar,
        int16_t**: int16_tstarstar,
        const int16_t*: constint16_tstar,
        const int16_t**: constint16_tstarstar,

        int32_t: int32_t,
        int32_t*: in32_tstar,
        int32_t**: int32_tstarstar,
        const int32_t*: constint32_tstar,
        const int32_t**: constint32_tstarstar,

        int64_t: int64_t,
        int64_t*: int64_tstar,
        int64_t**: int64_tstarstar,
        const int64_t*: constint64_tstar,
        const int64_t**: constint64_tstarstar,
    )
#endif

#endif
