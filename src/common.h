#ifndef GNURADIO_C_BLOCK_H
#define GNURADIO_C_BLOCK_H

#include <stddef.h>

#include "namespace.h"

#define _define_grc_BorrowedSizedArray(T, Ts) \
    struct NAMESPACE ## _BorrowedSizedArray_ ## Ts { \
        const T *values, \
        size_t size, \
    };
#define _generic_define(T, Ts) _define_grc_BorrowedSizedArray(T, Ts) 
#include "generics_commontypes.h"
#undef _generic_define




#endif
