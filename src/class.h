#include <stdbool.h>
#include <stddef.h>

#ifndef GRC_CLASS_H
#define GCC_CLASS_H

#define _methodprefix grc_basic_block_


#define _generic_func_method1(return_type, SelfType, prefix, name, ...) \
    return_type prefix ## name ## (SelfType *self, __VA_ARGS__)

#define _generic_func_method0(return_type, prefix, name) \
    return_type prefix ## name ## (SelfType *self)


#define _generic_funcptr_method1(return_type, SelfType, name, ...) \
    return_type (name)(SelfType *self, __VA_ARGS__)

#define _generic_funcptr_method0(return_type, name) \
    return_type (name)(SelfType *self)



#define _funcptr_method1(return_type, name, ...) \
    _generic_funcptr_method1(return_type, Self, name, __VA_ARGS__)

#define _funcptr_method0(return_type, name) \
    _generic_funcptr_method0(return_type, Self, name)


#define _func_method1(return_type, name, ...) \
    _generic_func_method1(return_type, Self, _methodprefix, name, __VA_ARGS__)

#define _func_method0(return_type, name) \
    _generic_func_method0(return_type, Self, _methodprefix, name)


#define DECLARE_CLASS_VTABLE(Self, methods) \
    struct _vtable_ ## Self { \
        methods \
    };

#define DECLARE_CLASS_FUNCTIONS(Self, methods) methods

#endif
