#ifndef GRC_SYNC_BLOCK_H
#define GRC_SYNC_BLOCK_H

#include <stdbool.h>
#include <stddef.h>

#include <grc/pmt.h>

#include "namespace.h" // should be last include

#define Self NAMESPACE ## _SyncBlock
#define _methodprefix NAMESPACE ## _sync_block

#define VIRTUAL_METHODS \
    method1(int, work, int noinput_items, const void **input_items, void **output_items);

#define METHODS \
    VIRTUAL_METHODS
    method1(void, forecast, int noinput_items, int *items_required, size_t items_required_size); \
    method1(int, general_work, int noinput_items, int *ninput_items, size_t ninput_items_size, const void **input_items, void **output_items); \
    method1(int, fixed_rate_ninput_to_noutput, int ninput); \
    method1(int, fixed_rate_noutput_to_ninput, int noutput); \
    method0(void, _protected_sync_block); \
    method1(void, _protected_sync_block_with, const char *name, grc_sptr_IoSignature input_signature, grc_sptr_IoSignature output_signature);
    // TODO define grc_sptr generic type shared pointer (from boost)

#ifdef __cplusplus
extern "C" {
#endif

#define method0(...) _funcptr_method0(__VA_ARGS__)
#define method1(...) _funcptr_method1(__VA_ARGS__)
DECLARE_CLASS_VTABLE(Self, VIRTUAL_METHODS)

#define method0(...) _func_method0(__VA_ARGS__)
#define method1(...) _func_method1(__VA_ARGS__)
DECLARE_CLASS_FUNCTIONS(Self, METHODS)

typedef struct Self {
    struct _vtable_ ## Self vtable;
    void *_data;
} Self;

#ifdef __cplusplus
}
#endif

#undef _methodprefix
#undef Self
#undef NAMESPACE

#endif
