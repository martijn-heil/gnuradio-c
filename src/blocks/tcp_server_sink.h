#ifndef GRC_BLOCKS_TCP_SERVER_SINK_H
#define GRC_BLOCKS_TCP_SERVER_SINK_H

#include <stdbool.h>
#include <stddef.h>

#include <grc/block.h>

#include "namespace.h" // should be last include

#define Self NAMESPACE ## _TcpServerSink
#define _methodprefix NAMESPACE ## _tcp_server_sink

//#define METHODS

#ifdef __cplusplus
extern "C" {
#endif

typedef struct Self {
    //struct _vtable_ ## Self vtable;
    struct grc_Block _super_Block;
    struct grc_BasicBlock _super_BasicBlock;
    struct grc_MsgAccepter _super_MsgAccepter;
    
    // TODO other inheritances
    void *_data;
} Self;

//#define method0(...) _funcptr_method0(__VA_ARGS__)
//#define method1(...) _funcptr_method1(__VA_ARGS__)
//DECLARE_CLASS_VTABLE(Self, METHODS)

//#define method0(...) _func_method0(__VA_ARGS__)
//#define method1(...) _func_method1(__VA_ARGS__)
//DECLARE_CLASS_FUNCTIONS(Self, METHODS)

Self _methodprefix ## _new(size_t itemsize, const char *host, int port, bool noblock);

#ifdef __cplusplus
}
#endif

#undef _methodprefix
#undef Self
#undef NAMESPACE

#endif
