#ifndef GRC_MOD_H
#define GRC_MOD_H

#include "namespace.h"


#define Self NAMESPACE ## _Work
enum Self {
    Self ## _DONE = -1,
    Self ## _CALLED_PRODUCE = -2
}
#undef Self
// So you get for example: grc_Work_CALLED_PRODUCE

#define Self NAMESPACE ## _TagPropagationPolicy
enum Self {
    Self ## _DONT        = 0,
    Self ## _ALL_TO_ALL  = 1,
    Self ## _ONE_TO_ONE  = 2,
    Self ## _CUSTOM      = 3
}
#undef Self
// So you get for example: grc_TagPropagationPolicy_ALL_TO_ONE

#undef NAMESPACE
#endif
