#include <gnuradio/block.h>

#include <gnuradio_c/block.h>

// Virtual public member functions:
void block_forecast(Block *self, int noutput_items, int *ninput_items_required, size_t n) 
{
    auto vec = vector<int> values(ninput_items_required, ninput_items_required + n);
    reinterpret_cast<gr::block*>(self)->forecast(noutput_items, vec);
}

int block_general_work(Block *self, int noutput_items, int *ninput_items, size_t ninput_items_size,         const void **input_items, size_t input_items_size, void **output_items, size_t output_items_size)
{
    auto vec_ninput_items = vector<int> values(ninput_items, ninput_items + ninput_items_size);
    auto vec_input_items = vector<void*> values(input_items, input_items + input_items_size);
    auto vec_output_items = vector<void*> values(output_items, output_items + output_items_size);
    return reinterpret_cast<gr::block*>(self)->general_work(noutput_items, vec_ninput_items, 
        vec_input_items, vec_output_items);
}

bool block_start(Block *self)
{
    try
    {
        return reinterpret_cast<gr::block*>(self)->start();
    } 
    catch(...)
    {
        fprintf(stderr, "Unexpected exception\n");
        abort();
    }
}

bool block_stop(Block *self)
{
    try
    {
        return reinterpret_cast<gr::block*>(self)->stop();
    }
    catch(...)
    {
        fprintf(stderr, "Unexpected exception\n");
        abort();
    }
}

int block_fixed_rate_ninput_to_noutput(Block *self, int ninput)
{
    try
    {
        return reinterpret_cast<gr::block*>(self)->fixed_rate_ninput_to_noutput(ninput);
    }
    catch(...)
    {
        printf(stderr, "Unexpected exception\n");
        abort();
    }
    
}

int block_fixed_rate_noutput_to_ninput(Block *self, int noutput)
{
    try
    {
        return reinterpret_cast<gr::block*>(self)->fixed_rate_noutput_to_ninput(noutput);
    }
    catch(...)
    {
        printf(stderr, "Unexpected exception\n");
        abort();
    }
}


// Public member functions

unsigned int block_history(Block *self)
{
    try
    {
        return reinterpret_cast<gr::block*>(self)->history();
    }
    catch(...)
    {
        printf(stderr, "Unexpected exception\n");
        abort();
    }
}

void block_set_history(Block *self)
{
    try
    {
        reinterpret_cast<gr::block*>(self)->set_history();
    }
    catch(...)
    {
        printf(stderr, "Unexpected exception\n");
        abort();
    }
}

void block_declare_sample_delay_which(Block *self, int which, unsigned int delay)
{
    try
    {
        reinterpret_cast<gr::block*>(self)->declare_sample_delay(which, delay);
    }
    catch(...)
    {
        printf(stderr, "Unexpected exception\n");
        abort();
    }
}

void block_declare_sample_delay(Block *self, unsigned int delay)
{
    try
    {
        reinterpret_cast<gr::block*>(self)->declare_sample_delay(delay);
    }
    catch(...)
    {
        printf(stderr, "Unexpected exception\n");
        abort();
    }
}

bool block_fixed_rate(Block *self)
{
    try
    {
        return reinterpret_cast<gr::block*>(self)->fixed_rate();
    }
    catch(...)
    {
        printf(stderr, "Unexpected exception\n");
        abort();
    }
}

void block_set_output_multiple(Block *self, int multiple)
{
    try
    {
        reinterpret_cast<gr::block*>(self)->set_output_multiple(multiple);
    }
    catch(...)
    {
        printf(stderr, "Unexpected exception\n");
        abort();
    }
}

int block_output_multiple(Block *self)
{
    try
    {
        return reinterpret_cast<gr::block*>(self)->output_multiple();
    }
    catch(...)
    {
        printf(stderr, "Unexpected exception\n");
        abort();
    }
}

bool block_output_multiple_set(Block *self)
{
    try
    {
        return reinterpret_cast<gr::block*>(self)->output_multiple_set();
    }
    catch(...)
    {
        printf(stderr, "Unexpected exception\n");
        abort();
    }
}

void block_set_alignment(Block *self, int multiple)
{
    try
    {
        reinterpret_cast<gr::block*>(self)->set_alignment(multiple);
    }
    catch(...)
    {
        printf(stderr, "Unexpected exception\n");
        abort();
    }
}

int block_unaligned(Block *self)
{
    try
    {
        return reinterpret_cast<gr::block*>(self)->unaligned();
    }
    catch(...)
    {
        printf(stderr, "Unexpected exception\n");
        abort();
    }
}

bool block_is_unaligned(Block *self)
{
    try
    {
        return reinterpret_cast<gr::block*>(self)->is_unaligned();
    }
    catch(...)
    {
        printf(stderr, "Unexpected exception\n");
        abort();
    }
}

void block_consume(Block *self, int which_input, int how_many_items);
void block_consume_each(Block *self, int how_many_items);
void block_produce(Block *self, int which_output, int how_many_items);
void block_set_relative_rate(Block *self, double relative_rate);
double block_relative_rate(Block *self, double relative_rate);
uint64_t block_nitems_read(Block *self, unsigned int which_input);
uint64_t block_nitems_written(Block *self, unsigned int which_output);
enum tag_propagation_policy block_tag_propagation_policy(Block *self);
void block_set_tag_propagation_policy(Block *self, enum tag_propagation_policy p);
int block_min_noutput_items(Block *self);
void block_set_min_noutput_items(Block *self, int m);
int block_max_noutput_items(Block *self);
void block_set_max_noutput_items(Block *self, int m);
void block_unset_max_noutput_items(Block *self);
bool block_is_set_max_noutput_items(Block *self);
void block_expand_minmax_buffer(Block *self, int port);
long block_max_output_buffer(Block *self, size_t i);
void block_set_max_output_buffer(Block *self, long max_output_buffer);
void block_set_max_output_buffer_with_port(Block *self, int port, long max_output_buffer);
long block_min_output_buffer(Block *self, size_t i);
void block_set_min_output_buffer(Block *self, long min_output_buffer);
float block_pc_noutput_items(Block *self);
float block_pc_noutput_items_avg(Block *self);
float block_pc_noutput_items_var(Block *self);
float block_pc_nproduced(Block *self);
float block_pc_nproduced_avg(Block *self);
float block_pc_nproduced_var(Block *self);
float block_pc_input_buffers_full(Block *self, int which);
float block_pc_input_buffers_full_avg(Block *self, int which);
float block_pc_input_buffers_full_var(Block *self, int which);
// TODO block_pc_input_buffers_full_* overloaded functions

float block_pc_output_buffers_full(Block *self, int which);
float block_pc_output_buffers_full_avg(Block *self, int which);
float block_pc_output_buffers_full_var(Block *self, int which);
// TODO block_pc_output_buffers_full_* overloaded functions

float block_pc_work_time(Block *self);
float block_pc_work_time_avg(Block *self);
float block_pc_work_time_var(Block *self);
float block_pc_work_time_total(Block *self);

float block_pc_throughput_avg(Block *self);
void block_reset_perf_counters(Block *self);
void block_setup_pc_rpc(Block *self);
bool block_is_pc_rpc_set(Block *self);
void block_no_pc_rpc(Block *self);
void block_set_processor_affinity(Block *self, int *mask, size_t n);
void block_unset_processor_affinity(Block *self);
int block_active_thread_priority(Block *self);
int block_thread_priority(Block *self);
int block_set_thread_priority(Block *self, int priority);
bool block_update_rate(Block *self);
// TODO void block_system_handler()
bool block_finished(Block *self);
// TODO block_detail & set_detail
void block_notify_msg_neighbors(Block *self);
void block_clear_finished(Block *self);
