#ifndef GRC_BASIC_BLOCK_H
#define GRC_BASIC_BLOCK_H

#include <stdbool.h>
#include <stddef.h>

#include "namespace.h" // should be last include

#define Self NAMESPACE ## _BasicBlock
#define _methodprefix NAMESPACE ## _basic_block_

#define METHODS \
    // TODO message_subscribers
    method0(long, unique_id); \
    method0(long, symbolic_id); \
    method0(char*, name); \
    method0(char*, symbol_name); \
    // TODO input_signature & output_signature
    // TODO to_basic_block
    method0(bool, alias_set); \
    method0(char* alias); \
    // TODO alias_pmt
    method1(void, set_block_alias, const char *name); \
    // TODO message_port*
    method0(void, setup_rpc); \
    method0(bool, is_rpc_set); \
    method0(void, rpc_set); \
    method1(bool, check_topology, int ninputs, int noutputs); \
    method1(void, set_processor_affinity, int *mask, size_t n); \
    method0(void, unset_processor_affinity); \
    method0(GrcBorrowedArray(int), processor_affinity); \
// end #define METHODS

#ifdef __cplusplus
extern "C" {
#endif

#define method0(...) _funcptr_method0(__VA_ARGS__)
#define method1(...) _funcptr_method1(__VA_ARGS__)
DECLARE_CLASS_VTABLE(Self, METHODS)

#define method0(...) _func_method0(__VA_ARGS__)
#define method1(...) _func_method1(__VA_ARGS__)
DECLARE_CLASS_FUNCTIONS(Self, METHODS)

#ifdef __cplusplus
}
#endif

#undef _methodprefix
#undef Self
#undef NAMESPACE

#endif
